import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr('string'),
	type: DS.attr('number'),
	syncthing_id: DS.attr('number')
});
