import Ember from 'ember';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

export default Ember.Route.extend(UnauthenticatedRouteMixin, {
	session: Ember.inject.service('session'),
	actions: {
		login() {
			let { username, password } = this.get('controller').getProperties('username', 'password');
			this.get('session').authenticate('authenticator:django', username, password)
			// .then(() => this.transitionTo('private'))
			.then(null, (reason) => {
				alert(reason.error);
			});
		}
	}
});
